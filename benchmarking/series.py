from benchmarking.track import *


class Series:
    data = []
    counter = 0
    n_tracks = 1
    # Config
    n_var = n_qu = n_cl = cl_len = to = to_lim = 0

    class Data:
        name = ""
        results = []
        times = []
        memory = []

        def __init__(self, name):
            self.name = name
            self.results = []
            self.times = []
            self.memory = []

        def add_data(self, result, time, mem):
            self.results.append(result)
            self.times.append(time)
            self.memory.append(mem)

        def log(self):
            log = str(self.name) + "\n"
            log += "Avg time: " + str(self.get_average_time()) + "\n"
            log += "Avg memory: " + str(self.get_average_memory()) + "\n"
            log += "Max time: " + str(max(self.times)) + "\n"
            log += "Min time: " + str(min(self.times)) + "\n"
            log += "Max memory: " + str(max(self.memory)) + "\n"
            log += "Min memory: " + str(self.get_min_mem()) + "\n\n"
            if None in self.results:
                log += "Unsolved problems: " + str(self.results.count(None)) + "\n"
            log += "Satisfiable: " + str(self.results.count(True)) + "\n"
            log += "Unsatisfiable: " + str(self.results.count(False)) + "\n"
            log += str(self.results) + "\n"
            log += str(self.times) + "\n"
            log += str(self.memory) + "\n\n"
            return log

        def get_min_mem(self):
            s = float("inf")
            for m in self.memory:
                if m != 0:
                    if m < s:
                        s = m
            return s

        def get_average_time(self):
            result = 0
            for t in self.times:
                result += t
            if (len(self.times) - self.times.count(0)) == 0:
                return 0
            return result / (len(self.times) - self.times.count(0))

        def get_average_memory(self):
            result = 0
            for m in self.memory:
                result += m
            if (len(self.memory) - self.memory.count(0)) == 0:
                return 0
            return result / (len(self.memory) - self.memory.count(0))

    def __init__(self, n_var, n_qu, n_cl, cl_len, to, to_lim=3, n_tracks=1):
        self.data = []
        self.counter = 0
        self.n_tracks = n_tracks
        self.n_var = n_var
        self.n_qu = n_qu
        self.n_cl = n_cl
        self.cl_len = cl_len
        self.to = to
        self.to_lim = to_lim

    def run_sat_track(self, name="", formula=None):
        from gen.formats import Dimacs
        # SAT TRACK
        if formula is None:
            formula = Dimacs(self.n_var, self.n_cl, self.cl_len)
        t = Track("sat_crypto" + str(name), formula, self.to, self.to_lim)
        t.run(Track.crypto_minisat)
        t.log_data()
        t.run(Track.sat_resolve)
        t.log_data()
        t.run(Track.qbf_resolve)
        t.log_data()

    def run_qbf_track(self, name="", formula=None):
        # QBF TRACK
        if formula is None:
            formula = Qdimacs(self.n_var, self.n_qu, self.n_cl, self.cl_len)
        t = Track("qbf" + str(name), formula, self.to, self.to_lim)
        t.run(Track.qbf_resolve)
        t.log_data()
        t.run(Track.qbf_reduce)
        t.log_data()

    def run_qbf_series(self, q_res, q_red):
        resolution_data = self.Data("qbf_resolution")
        reduction_data = self.Data("qbf_reduction")
        for k in range(0, self.n_tracks):
            # QBF TRACK
            t = Track("qbf" + str(k), Qdimacs(self.n_var, self.n_cl, self.n_qu, self.cl_len), self.to, self.to_lim)
            if q_res:
                t.run(Track.qbf_resolve)
                resolution_data.add_data(*self.get_track_data(t))
            if q_red:
                t.run(Track.qbf_reduce)
                reduction_data.add_data(*self.get_track_data(t))
        self.data = []
        if q_res:
            self.data.append(resolution_data)
        if q_red:
            self.data.append(reduction_data)
        self.log_series("qbf")
        if q_res:
            res_time = resolution_data.get_average_time()
            res_mem = resolution_data.get_average_memory()
        else:
            res_time = None
            res_mem = None
        if q_red:
            red_time = reduction_data.get_average_time()
            red_mem = reduction_data.get_average_memory()
        else:
            red_time = None
            red_mem = None

        return res_time, res_mem, red_time, red_mem

    def run_qres_series(self, qres_1=True, qres_2=True):
        resolution_data = self.Data("qbf_res_most_often_first")
        res2_data = self.Data("qbf_res_most_often_last")
        for k in range(0, self.n_tracks):
            # QBF TRACK
            t = Track("qbf" + str(k), Qdimacs(self.n_var, self.n_cl, self.n_qu, self.cl_len), self.to, self.to_lim)
            t.run(Track.qbf_resolve)
            resolution_data.add_data(*self.get_track_data(t))
            t.run(Track.qbf_resolve_alternative)
            res2_data.add_data(*self.get_track_data(t))
        self.data = [resolution_data, res2_data]
        self.log_series("qbf")
        return resolution_data.get_average_time(), resolution_data.get_average_memory(), \
               res2_data.get_average_time(), res2_data.get_average_memory()

    def run_sat_series(self, cry, dp, qres):
        cryptosat_data = self.Data("cryptominisat")
        sat_resolution_data = self.Data("sat_resolution")
        qbf_resolution_data = self.Data("qbf_resolution")
        for k in range(0, self.n_tracks):
            # SAT TRACK
            t = Track("sat" + str(k), Qdimacs(self.n_var, self.n_cl, self.n_qu, self.cl_len), self.to, self.to_lim)
            # CryptoMiniSat
            if cry:
                t.run(Track.crypto_minisat)
                cryptosat_data.add_data(*self.get_track_data(t))
            # DavisPutnam Resolution
            if dp:
                t.run(Track.sat_resolve)
                sat_resolution_data.add_data(*self.get_track_data(t))
            # Q-Resolution
            if qres:
                t.run(Track.qbf_resolve)
                qbf_resolution_data.add_data(*self.get_track_data(t))
        self.data = []
        if cry:
            self.data.append(cryptosat_data)
        if dp:
            self.data.append(sat_resolution_data)
        if qres:
            self.data.append(qbf_resolution_data)
        self.log_series("sat")
        cry_time = cry_mem = dp_time = dp_mem = qres_time = qres_mem = 0
        if cry:
            cry_time = cryptosat_data.get_average_time()
            cry_mem = cryptosat_data.get_average_memory()
        if dp:
            dp_time = sat_resolution_data.get_average_time()
            dp_mem = sat_resolution_data.get_average_memory()
        if qres:
            qres_time = qbf_resolution_data.get_average_time()
            qres_mem = qbf_resolution_data.get_average_memory()
        return cry_time, cry_mem, dp_time, dp_mem, qres_time, qres_mem

    @staticmethod
    def get_track_data(t):
        results_equal, result, any_timeouts, time_median, memory_median = t.get_data()
        if results_equal:
            pass
        else:
            result = "Ambivalent"
        return result, time_median, memory_median

    def log_series(self, name):
        print("Logging to file..")
        file_name = str(get_project_src()) + "/files/logs/" + str(name) + "_v" + str(self.n_var) + "_c" + \
                    str(self.n_cl) + "_l" + str(self.cl_len) + '_series.log'
        print(file_name)
        log = open(file_name, 'w+')
        log.write("#Software: QBF/SAT-Algorithms\n"
                  "#Date: " + datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S") + "\n\n")
        log.write(self.setup_string())
        for d in self.data:
            log.write(d.log())
        log.close()

    def get_coordinates(self, x):
        coordinates = ""
        for d in self.data:
            coordinates += "Average Time: "
            coordinates += "( " + ", " + str(d.get_average_time()) + ")\n"
            coordinates += "Average Memory: "
            coordinates += "( " + ", " + str(d.get_average_memory()) + ")\n"
        return coordinates

    def setup_string(self):
        setup = "Track Series\n\n"
        setup += "Number of vars:" + str(self.n_var) + "\n"
        setup += "Number of quantifiers:" + str(self.n_qu) + "\n"
        setup += "Number of clauses:" + str(self.n_cl) + "\n"
        setup += "Length of clauses:" + str(self.cl_len) + "\n"
        setup += "Timeout:" + str(self.to) + "\n"
        setup += "Max no timeouts:" + str(self.to_lim) + "\n\n"
        return setup


def main():
    series = Series(n_var=5, n_qu=0, n_cl=4, cl_len=3, to=2, to_lim=3, n_tracks=1)
    cry_time, cry_mem, dp_time, dp_mem, qres_time, qres_mem = series.run_sat_series(False, True, True)
    series = Series(n_var=5, n_qu=5, n_cl=4, cl_len=3, to=2, to_lim=3, n_tracks=1)
    res1_time, res1_mem, res2_time, res2_mem = series.run_qres_series(True, True)


if __name__ == "__main__":
    main()
