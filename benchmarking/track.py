import datetime
import linecache
import tracemalloc

from algorithms.cryptominisat import run_cryptosat
from algorithms.qreduction.qbf_naive import qbf_naive
from algorithms.qresolution2.q_solve import Q_Solve
from algorithms.resolution import dp_res
from benchmarking.timer import func_with_bench
from gen.formats import Qdimacs
from util.path import get_project_src


class Track:
    name = "UNNAMED"
    timeout_limit = 0
    # CONFIGURATION
    timeout = 0
    repetitions = 20
    formula = Qdimacs()

    nbvar = nbquant = nbclauses = clause_length = 0

    # DATA & RESULTS
    function = ""
    times = []
    timeouts = 0
    memory_usage = []
    results = []

    # LOGS
    first_time_opening = True

    def __init__(self, name, formula=Qdimacs(), timeout=20, timeout_limit=0):
        self.name = name
        self.set_formulas(formula)
        self.timeout = timeout
        self.timeout_limit = timeout_limit

    def set_formulas(self, formula):
        self.formula = formula
        self.nbvar = formula.nbvar
        self.nbquant = formula.nbquant
        assert formula.nbquant <= formula.nbvar
        self.nbclauses = formula.nbclauses
        self.clause_length = formula.clause_length
        if "sat" in self.name:
            self.formula.to_dimacs().write_dimacs_file(str(get_project_src()) + "/files/test_sets/" + self.name + "_" +
                                                       str(self.formula.nbvar) + "_" + str(self.formula.nbclauses)
                                                       + "_", datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))
        else:
            self.formula.write_qdimacs_file(
                str(get_project_src()) + "/files/test_sets/" + self.name + "_" +
                str(self.formula.nbvar) + "_" + str(self.formula.nbclauses) + "_",
                datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))

    def set_repetitions(self, n):
        self.repetitions = n

    def log_data(self):
        import os
        print("Logging to file..")
        file_name = str(get_project_src()) + "/files/logs/" + str(self.name) + "_v" + str(self.nbvar) + "_c" + \
                    str(self.nbclauses) + "_l" + str(self.clause_length) + '_track.log'
        if os.path.exists(file_name):
            append_write = 'a'  # append if already exists
        else:
            append_write = 'w'  # make a new file if not
        if self.first_time_opening:
            self.first_time_opening = False
            log = open(file_name, append_write)
            log.write("#Software: QBF/SAT-Algorithms\n"
                      "#Date: " + datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S") + "\n\n")
            log.write(self.setup_to_string() + "\n")
        else:
            log = open(file_name, 'a')
            log.write("\n")
        log.write("Algorithm: " + self.function + "\n")
        log.write(self.results_to_string())
        log.write(self.time_data_to_string())
        log.write(self.memory_data_to_string())
        log.close()

    def setup_to_string(self):
        setup = "---SETUP---\n"
        setup += str(self.repetitions) + " repetitions for (Q)DIMACS instance with"
        setup += " nbvar: " + str(self.nbvar)
        setup += ", nbclauses: " + str(self.nbclauses)
        setup += ", nbquant: " + str(self.nbquant)
        setup += ", clause length: " + str(self.clause_length) + "\n"
        if self.nbquant == 0:
            formula_string = "Formula:\n" + self.formula.to_string()
        else:
            formula_string = "Formula:\n" + self.formula.to_string()
        if len(formula_string) > 1000:
            formula_string = formula_string[0:999] + "..."
        setup += formula_string
        return setup + "\n"

    def results_to_string(self):
        all_results_equal = self.get_all_results_equal()
        result = "---RESULTS--\n"
        result += "All results equal: " + str(all_results_equal) + "\n"
        if all_results_equal:
            result += "Result: " + str(self.results[0]) + "\n"
        else:
            result += "All results: " + str(self.results) + "\n"
        return result

    def get_all_results_equal(self):
        value = self.results[0]
        for result in self.results:
            if value != result:
                return False
        return True

    def time_data_to_string(self):
        data = "---TIME---\n"
        data += "Min: " + str(min(self.times)) + "\n"
        data += "Max: " + str(max(self.times)) + "\n"
        data += "Median: " + str(self.get_median(self.times)) + "\n"
        data += "Highest difference(max-min): " + str(max(self.times) - min(self.times)) + "\n"
        data += "Greatest Difference % (max from min): " + str(
            ((max(self.times) - min(self.times)) / min(self.times)) * 100) + "\n"
        data += "Timeouts: " + str(self.timeouts) + "\n"
        data += "All: " + str(self.times) + "\n"
        return data

    def memory_data_to_string(self):
        data = "---MEMORY---\n"
        data += "Min usage: " + str(min(self.memory_usage)) + "\n"
        data += "Max usage: " + str(max(self.memory_usage)) + "\n"
        data += "All usage: " + str(self.memory_usage) + "\n"
        return data

    def run(self, function):
        self.function = str(function.__name__)
        print("Starting track for " + self.function + "!")
        self.times = []
        self.timeouts = 0
        self.memory_usage = []
        self.results = []
        for i in range(0, self.repetitions):
            timed_out, time, result, peak = func_with_bench(function, [self], self.timeout)
            self.times.append(time)
            self.memory_usage.append(peak)
            self.results.append(result)
            if timed_out:
                self.timeouts += 1
                if self.timeouts >= self.timeout_limit:
                    break

    def qbf_reduce(self):
        # mem = memory_usage((func_with_timeout, (solve_q_res, [formula], timeout)), multiprocess=True)
        tracemalloc.start()
        result = qbf_naive(self.formula)
        current, peak = tracemalloc.get_traced_memory()
        print(f"Current memory usage is {current / 10 ** 6}MB; Peak was {peak / 10 ** 6}MB")
        snapshot = tracemalloc.take_snapshot()
        tracemalloc.stop()
        self.display_top(snapshot)
        return result, peak, None

    def qbf_resolve(self):
        tracemalloc.start()
        solv = Q_Solve(self.formula)
        result = solv.solve_q_res()
        current, peak = tracemalloc.get_traced_memory()
        # print(f"Current memory usage is {current / 10 ** 6}MB; Peak was {peak / 10 ** 6}MB")
        snapshot = tracemalloc.take_snapshot()
        tracemalloc.stop()
        self.display_top(snapshot)
        return result, peak, None

    def qbf_resolve_alternative(self):
        tracemalloc.start()
        solv = Q_Solve(self.formula)
        result = solv.solve_q_res_alternative()
        current, peak = tracemalloc.get_traced_memory()
        snapshot = tracemalloc.take_snapshot()
        tracemalloc.stop()
        self.display_top(snapshot)
        return result, peak, None

    def crypto_minisat(self):
        file_name = str(get_project_src()) + "/files/temp/crypto_"
        file_id = 0
        run_cryptosat.write_file(self.formula, file_name, file_id)
        result, time, peak = run_cryptosat.run_crypto_sat(file_name, file_id)
        return result, peak, time

    def sat_resolve(self):
        # mem = memory_usage((func_with_timeout, (solve_q_res, [formula], timeout)), multiprocess=True)
        tracemalloc.start()
        result = dp_res.dp_res(self.nbvar, self.nbclauses, self.formula.clauses)
        current, peak = tracemalloc.get_traced_memory()
        print(f"Current memory usage is {current / 10 ** 6}MB; Peak was {peak / 10 ** 6}MB")
        snapshot = tracemalloc.take_snapshot()
        tracemalloc.stop()
        self.display_top(snapshot)
        return result, peak, None

    def get_data(self):
        results_equal = self.get_all_results_equal()
        if results_equal:
            result = self.results[0]
        else:
            result = None
        if self.timeouts > 0:
            any_timeouts = True
        else:
            any_timeouts = False
        time_median = self.get_median(self.times)
        memory_median = self.get_median(self.memory_usage)
        return results_equal, result, any_timeouts, time_median, memory_median

    @staticmethod
    def display_top(snapshot, key_type='lineno', limit=3):
        snapshot = snapshot.filter_traces((
            tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
            tracemalloc.Filter(False, "<unknown>"),
        ))
        top_stats = snapshot.statistics(key_type)

        print("Top %s lines" % limit)
        for index, stat in enumerate(top_stats[:limit], 1):
            frame = stat.traceback[0]
            print("#%s: %s:%s: %.1f KiB"
                  % (index, frame.filename, frame.lineno, stat.size / 1024))
            line = linecache.getline(frame.filename, frame.lineno).strip()
            if line:
                print('    %s' % line)

        other = top_stats[limit:]
        if other:
            size = sum(stat.size for stat in other)
            print("%s other: %.1f KiB" % (len(other), size / 1024))
        total = sum(stat.size for stat in top_stats)
        print("Total allocated size: %.1f KiB" % (total / 1024))

    @staticmethod
    def get_median(values):
        length = len(values)
        sorted_values = sorted(values)
        if len(sorted_values) % 2 == 0:
            return (sorted_values[int(length / 2) - 1] + sorted_values[int(length / 2)]) / 2
        else:
            return sorted_values[int(length / 2) - 1]


def main():
    from benchmarking.benching import call_sat_track, call_qbf_track
    call_sat_track(cry=False)
    call_qbf_track()
