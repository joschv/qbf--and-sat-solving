from algorithms.cryptominisat.run_cryptosat import write_file, run_crypto_sat
from benchmarking.series import *
from gen.formats import Dimacs, Qdimacs
from util.path import get_project_path


def call_sat_track(name="sat", formula=Dimacs(), cry=True, dp=True):
    # SAT TRACK
    timeout = 20
    timeout_limit = 5
    t = Track(name, formula, timeout, timeout_limit)
    t.set_repetitions(3)
    if cry:
        t.run(Track.crypto_minisat)
        t.log_data()
    if dp:
        t.run(Track.sat_resolve)
        t.log_data()


def call_qbf_track(name="qbf", formula=Qdimacs()):
    # QBF TRACK
    timeout = 20
    timeout_limit = 5
    t = Track(name, formula, timeout, timeout_limit)
    t.run(Track.qbf_resolve)
    t.log_data()
    t.run(Track.qbf_reduce)
    t.log_data()


def call_tracks():
    call_sat_track()
    call_qbf_track()


def formula_comparison():
    f1 = Qdimacs()
    f2 = Qdimacs()
    f1.import_file(str(get_project_src()) + "/files/qdimacs/study_1/qdimacs_0.qcnf")
    f2.import_file(str(get_project_src()) + "/files/qdimacs/study_1/qdimacs_1.qcnf")
    call_qbf_track("qbf_study_1", f1)
    call_qbf_track("qbf_study_2", f2)


def sat_result_comparison():
    results = []
    low = 10
    high = 32
    step = 2

    print("Starting log file..")
    file_name = str(get_project_src()) + "/files/test_results/satisfiability_sat_1/sat_" + str(low) + "-" \
                + str(high) + "_" + datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + ".log"
    string = "#Software: QBF/SAT-Algorithms\n#Date: " + datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S") \
             + "\n\n"
    log = open(file_name, 'w+')
    log.write(string)
    log.close()
    for i in range(low, high, step):
        one = [i, 0, 0, 0]
        for k in range(0, 1000):
            f = Dimacs(i, 100, 3)
            write_file(f, "temp_", 0)
            write_file(f, str(get_project_src()) + "/files/test_sets/satisfiability_sat_1/sat_" + str(f.nbvar) + "-"
                       + str(f.nbclauses) + "_", str(k))
            val, time, mem = run_crypto_sat("temp_", 0)
            if val is not None:
                if val:
                    one[1] += 1
                else:
                    one[2] += 1
                one[3] += 1
        satisfiable = one[1] / one[3]
        results.append(satisfiable)
        log = open(file_name, 'a')
        log.write("(" + str(i) + "," + str(satisfiable) + ")\n")
        log.close()
    print_list_as_plot(list(range(low, high, step)), results)


def qbf_result_comparison():
    results = []
    low = 1
    high = 2
    step = 1

    print("Starting log file..")
    file_name = str(get_project_src()) + "/files/test_results/satisfiability_qbf_1/qbf_" + str(low) + "-" \
                + str(high) + "_" + datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + ".log"
    string = "#Software: QBF/SAT-Algorithms\n#Date: " + datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S") \
             + "\n\n"
    log = open(file_name, 'w+')
    log.write(string)
    log.close()
    for i in range(low, high, step):
        one = [i, 0, 0, 0]
        for k in range(0, 1000):
            f = Qdimacs(nbvar=10, nbclauses=20, nbquant=10, clause_length=i)
            solv = Q_Solve(f)
            val = solv.solve_q_res()
            if val is not None:
                if val:
                    one[1] += 1
                else:
                    one[2] += 1
                one[3] += 1
        satisfiable = one[1] / one[3]
        results.append(satisfiable)
        log = open(file_name, 'a')
        log.write("(" + str(i) + "," + str(satisfiable) + ")\n")
        log.close()
    print_list_as_plot(list(range(low, high, step)), results)


def print_list_as_plot(xlist, ylist):
    assert len(xlist) == len(ylist)
    for k in range(0, len(xlist)):
        x = xlist[k]
        y = ylist[k]
        print("(" + str(x) + "," + str(y) + ")")


def start_log(file_name, string):
    print("Starting log file..")
    log = open(file_name, 'w+')
    log.write(string)
    log.close()


def iterate_series_qbf(q_res=True, q_red=True):
    low = 10
    high = 15
    step = 5
    string_res_time = ""
    string_res_mem = ""
    string_red_time = ""
    string_red_mem = ""
    file_name = str(get_project_src()) + "/files/logs/coordinates/qbf" + str(low) + "-" + str(high) \
                + "_" + datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + '_coordinates.log'
    string = "#Software: QBF/SAT-Algorithms\n#Date: " + datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S") \
             + "\n\n"
    start_log(file_name, string)
    for i in range(low, high, step):
        series = Series(n_var=3 * i, n_qu=3 * i, n_cl=i, cl_len=5, to=20, to_lim=3, n_tracks=50)
        res_time, res_mem, red_time, red_mem = series.run_qbf_series(q_res, q_red)
        # LOG
        coordinates = open(file_name, 'a')
        coordinates.write("\n")
        if q_res:
            coordinates.write("\nResolution time:\n")
            current_string_res_time = "(" + str(i) + "," + str(res_time) + ")\n"
            string_res_time += current_string_res_time
            coordinates.write(current_string_res_time)
            coordinates.write("\nResolution memory:\n")
            current_string_res_mem = "(" + str(i) + "," + str(res_mem) + ")\n"
            string_res_mem += current_string_res_mem
            coordinates.write(current_string_res_mem)
            if res_time > series.to - 0.1:
                q_res = False
        if q_red:
            coordinates.write("\nReduction time:\n")
            current_string_red_time = "(" + str(i) + "," + str(red_time) + ")\n"
            string_red_time += current_string_red_time
            coordinates.write(current_string_red_time)
            coordinates.write("\nReduction memory:\n")
            current_string_red_mem = "(" + str(i) + "," + str(red_mem) + ")\n"
            string_red_mem += current_string_red_mem
            coordinates.write(current_string_red_mem)
            if red_time > series.to - 0.1:
                q_red = False
        coordinates.close()
    # COMPLETE LOG
    file_name = str(get_project_src()) + "/files/logs/coordinates/qbf" + str(low) + "-" + str(high) \
                + "_" + datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + '_allcoordinates.log'
    print("Logging coordinates to file..")
    coordinates = open(file_name, 'w+')
    coordinates.write(
        "#Software: QBF/SAT-Algorithms\n#Date: " + datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")
        + "\n\n")
    if string_res_time != "":
        coordinates.write("\nResolution time:\n")
        coordinates.write(string_res_time)
        coordinates.write("\nResolution mem:\n")
        coordinates.write(string_res_mem)
    if string_red_time != "":
        coordinates.write("\nReduction time:\n")
        coordinates.write(string_red_time)
        coordinates.write("\nReduction mem:\n")
        coordinates.write(string_red_mem)
    coordinates.close()


def iterate_series_qres(q_res_1=True, q_res_2=True):
    low = 7
    high = 75
    step = 5
    string_res1_time = ""
    string_res1_mem = ""
    string_res2_time = ""
    string_res2_mem = ""
    file_name = str(get_project_src()) + "/files/logs/coordinates/qbf" + str(low) + "-" + str(high) \
                + "_" + datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + '_coordinates.log'
    string = "#Software: QBF/SAT-Algorithms\n#Date: " + datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S") \
             + "\n\n" + "QRes1: Resolve with most often appearing literal first.\nQRes2: Resolve with least often " \
                        "appearing literal first."
    start_log(file_name, string)
    for i in range(low, high, step):
        series = Series(n_var=3 * i, n_qu=3 * i, n_cl=i, cl_len=5, to=20, to_lim=3, n_tracks=50)
        res1_time, res1_mem, res2_time, res2_mem = series.run_qres_series(q_res_1, q_res_2)
        # LOG
        coordinates = open(file_name, 'a')
        coordinates.write("\n")
        if q_res_1:
            coordinates.write("\nRes1 time:\n")
            current_string_res1_time = "(" + str(i) + "," + str(res1_time) + ")\n"
            string_res1_time += current_string_res1_time
            coordinates.write(current_string_res1_time)
            coordinates.write("\nRes1 memory:\n")
            current_string_res1_mem = "(" + str(i) + "," + str(res1_mem) + ")\n"
            string_res1_mem += current_string_res1_mem
            coordinates.write(current_string_res1_mem)
            if res1_time > series.to - 0.1:
                q_res_1 = False
        if q_res_2:
            coordinates.write("\nRes2 time:\n")
            current_string_res2_time = "(" + str(i) + "," + str(res2_time) + ")\n"
            string_res2_time += current_string_res2_time
            coordinates.write(current_string_res2_time)
            coordinates.write("\nRes2 memory:\n")
            current_string_res2_mem = "(" + str(i) + "," + str(res2_mem) + ")\n"
            string_res2_mem += current_string_res2_mem
            coordinates.write(current_string_res2_mem)
            if res2_time > series.to - 0.1:
                q_res_2 = False
        coordinates.close()
    # COMPLETE LOG
    file_name = str(get_project_src()) + "/files/logs/coordinates/qbf" + str(low) + "-" + str(high) \
                + "_" + datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + '_allcoordinates.log'
    print("Logging coordinates to file..")
    coordinates = open(file_name, 'w+')
    coordinates.write(
        "#Software: QBF/SAT-Algorithms\n#Date: " + datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")
        + "\n\n")
    if string_res1_time != "":
        coordinates.write("\nRes1 time:\n")
        coordinates.write(string_res1_time)
        coordinates.write("\nRes1 mem:\n")
        coordinates.write(string_res1_mem)
    if string_res2_time != "":
        coordinates.write("\nRes2 time:\n")
        coordinates.write(string_res2_time)
        coordinates.write("\nRes2 mem:\n")
        coordinates.write(string_res2_mem)
    coordinates.close()


def iterate_series_sat(crypto=True, dp=True, q_res=True):
    low = 2000
    high = 11000
    step = 1000
    string_cry_time = ""
    string_cry_mem = ""
    string_dp_time = ""
    string_dp_mem = ""
    string_qres_time = ""
    string_qres_mem = ""
    file_name = str(get_project_src()) + "/files/logs/coordinates/sat" + str(low) + "-" + str(high) \
                + "_" + datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + '_coordinates.log'
    string = "#Software: QBF/SAT-Algorithms\n#Date: " + datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S") \
             + "\n\n"
    start_log(file_name, string)
    for i in range(low, high, step):
        series = Series(n_var=i, n_qu=0, n_cl=5 * i, cl_len=5, to=20, to_lim=3, n_tracks=10)
        cry_time, cry_mem, dp_time, dp_mem, qres_time, qres_mem = series.run_sat_series(crypto, dp, q_res)
        # LOG
        coordinates = open(file_name, 'a')
        coordinates.write("\n")
        if crypto:
            coordinates.write("\nCryptoMiniSat time:\n")
            current_string_cry_time = "(" + str(i) + "," + str(cry_time) + ")\n"
            string_cry_time += current_string_cry_time
            coordinates.write(current_string_cry_time)
            coordinates.write("\nCryptoMiniSat memory:\n")
            current_string_cry_mem = "(" + str(i) + "," + str(cry_mem) + ")\n"
            string_cry_mem += current_string_cry_mem
            coordinates.write(current_string_cry_mem)
        if dp:
            coordinates.write("\nDavis-Putnam time:\n")
            current_string_dp_time = "(" + str(i) + "," + str(dp_time) + ")\n"
            string_dp_time += current_string_dp_time
            coordinates.write(current_string_dp_time)
            coordinates.write("\nDavis-Putnam memory:\n")
            current_string_dp_mem = "(" + str(i) + "," + str(dp_mem) + ")\n"
            string_dp_mem += current_string_dp_mem
            coordinates.write(current_string_dp_mem)
        if q_res:
            coordinates.write("\nQ-Resolution time:\n")
            current_string_qres_time = "(" + str(i) + "," + str(qres_time) + ")\n"
            string_qres_time += current_string_qres_time
            coordinates.write(current_string_qres_time)
            coordinates.write("\nQ-Resolution memory:\n")
            current_string_qres_mem = "(" + str(i) + "," + str(qres_mem) + ")\n"
            string_qres_mem += current_string_qres_mem
            coordinates.write(current_string_qres_mem)
        coordinates.close()
    # COMPLETE LOG
    file_name = str(get_project_src()) + "/files/logs/coordinates/sat" + str(low) + "-" + str(high) \
                + "_" + datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + '_allcoordinates.log'
    print("Logging coordinates to file..")
    coordinates = open(file_name, 'w+')
    coordinates.write(
        "#Software: QBF/SAT-Algorithms\n#Date: " + datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")
        + "\n\n")
    coordinates.write("\nCryptoMiniSat time:\n")
    coordinates.write(string_cry_time)
    coordinates.write("\nCryptoMiniSat mem:\n")
    coordinates.write(string_cry_mem)
    coordinates.write("\nDavis-Putnam time:\n")
    coordinates.write(string_dp_time)
    coordinates.write("\nDavis-Putnam mem:\n")
    coordinates.write(string_dp_mem)
    coordinates.write("\nQ-Resolution time:\n")
    coordinates.write(string_qres_time)
    coordinates.write("\nQ-Resolution mem:\n")
    coordinates.write(string_qres_mem)
    coordinates.close()


def run_series_on_test_set(query="sat", subfolder=""):
    import glob
    path = str(get_project_src()) + "/files/test_sets/"
    if subfolder != "":
        path += str(subfolder) + "/"
    files = glob.glob(path + "*" + str(query) + "*")
    n_files = len(files)
    print("Files found: " + str(n_files))
    res_times = []
    res_memory = []
    red_times = []
    red_memory = []
    for file in files:
        if file.endswith(".cnf"):
            f = Dimacs()
            f.import_file(file)
            call_sat_track("sat" + str(subfolder.replace("/", "")), f)
        elif file.endswith(".qcnf"):
            f = Qdimacs()
            f.import_file(file)
            timeout = 20
            timeout_limit = 5
            t = Track("qbf", f, timeout, timeout_limit)
            t.run(Track.qbf_resolve)
            res_times.append(t.get_median(t.times))
            res_memory.append(t.get_median(t.memory_usage))
            # t.run(Track.qbf_reduce)
            # red_times.append(t.get_median(t.times))
            # red_memory.append(t.get_median(t.memory_usage))
        else:
            print("Trying to evaluate file that is neither .cnf nor .qcnf")
            n_files -= 1
            continue
    res_time_avg = sum(res_times) / len(res_times)
    while 0 in res_memory:
        res_memory.remove(0)
    res_mem_avg = sum(res_memory) / len(res_memory)
    print("Files evaluated: ", n_files)
    print("Resolution Average time: ", res_time_avg)
    print("Resolution Average memory: ", res_mem_avg)

    # red_time_avg = sum(red_times) / len(red_times)
    # while 0 in red_memory:
    #     red_memory.remove(0)
    # red_mem_avg = sum(red_memory) / len(red_memory)
    # print("Files evaluated: ", n_files)
    # print("Reduction Average time: ", red_time_avg)
    # print("Reduction Average memory: ", red_mem_avg)


def satisfiability_of_test_set(query="sat", subfolder=""):
    import glob
    path = get_project_path() + "/files/test_sets/"
    if subfolder != "":
        path += str(subfolder) + "/"
    files = glob.glob(path + "*" + str(query) + "*")
    n_files = len(files)
    n_true = 0
    print("Files found: " + str(n_files))
    for file in files:
        if file.endswith(".cnf"):
            val = run_cryptosat.get_satifiability(file)
        elif file.endswith(".qcnf"):
            formula = Qdimacs()
            formula.import_file(file)
            solv = Q_Solve(formula)
            val = solv.solve_q_res()
        else:
            print("Trying to get satisfiability that is neither .cnf nor .qcnf")
            n_files -= 1
            continue
        if val:
            n_true += 1
    print("Files evaluated: ", n_files)
    print("Satisfiable formulas: ", n_true)
    if n_files > 0:
        sat_perc = n_true / n_files
    else:
        sat_perc = 0
    print("Sat%: ", sat_perc)
    return sat_perc


def create_test_set(satifiable=True, target=50):
    low = 45
    high = 65
    step = 5
    n_tracks = 20
    for i in range(low, high, step):
        found = 0
        found_new = True
        round = 0
        while found_new and found < target:
            found_new = False
            for j in range(0, n_tracks):
                formula = Qdimacs(nbvar=i, nbclauses=i, nbquant=i, clause_length=0)
                solv = Q_Solve(formula)
                val = solv.solve_q_res()
                if satifiable == val:
                    formula.write_qdimacs_file(str(get_project_src()) + "/files/test_sets/current/" + str(val) + "/" +
                                               str(i) + "/" + "qbf_" + str(val) + "_" + str(round) + "_", str(i) + "_"
                                               + str(j))
                    found += 1
                    found_new = True
                    if found == target:
                        break
            round += 1
        if found < target:
            print("Stopped before finding " + str(target) + " formulas. Only got " + str(found) + ".")
            return
    print("Found the " + str(target) + " formulas you wanted, master!")


def main():
    qbf_result_comparison()
    # sat_result_comparison()
    # formula_comparison()
    # for i in range(3, 11, 1):
    #    series = Series(n_var=i, n_qu=0, n_cl=5*i, cl_len=3, to=20, to_lim=3, n_tracks=10)
    #    #series.run_qbf_series()
    #    series.run_qres_series()
    #    #series.run_sat_series()
    # iterate_series_qbf(q_res=True, q_red=True)
    # iterate_series_sat(q_res=False, dp=False)
    # iterate_series_qres(q_res_1=True, q_res_2=True)
    # satisfiability_of_test_set(query="qbf", subfolder="current/false/5")
    # formula = Qdimacs()
    # formula.import_file(str(get_project_src()) + "/files/qdimacs/qbf35_22_22_12-05-2020_11-03-50.qcnf")
    # call_qbf_track("qbf-22", formula)
    # create_test_set(False, 20)
    # run_series_on_test_set("qbf", "current/true/60")


if __name__ == "__main__":
    main()
