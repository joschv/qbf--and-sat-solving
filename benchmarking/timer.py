import multiprocessing
import time

from algorithms.qresolution import qbf_solver
from gen import formats


def func_with_bench(function, args, timeout):
    timed_out = False
    print("Starting Timeout-Process, Timeout:", timeout)
    pool = multiprocessing.Pool(1)
    res = pool.apply_async(function, args)
    start_time = time.perf_counter()
    try:
        result, peak, t = res.get(timeout)
        current_time = time.perf_counter() - start_time
        print("Runtime:", current_time)
        print("Finished before Timeout!")
    except multiprocessing.TimeoutError:
        current_time = time.perf_counter() - start_time
        print("Runtime:", current_time)
        pool.terminate()
        print("Timed out!")
        timed_out = True
        result = None
        peak = 0
        t = None
    print("Result:", result)
    print("Ended Timeout-Process")
    pool.close()
    if t:
        return timed_out, t, result, peak
    return timed_out, current_time, result, peak


def func_with_timeout(function, args, timeout):
    timed_out = False
    print("Starting Timeout-Process, Timeout:", timeout)
    pool = multiprocessing.Pool(1)
    res = pool.apply_async(function, args)
    start_time = time.perf_counter()
    try:
        result = res.get(timeout)
        current_time = time.perf_counter() - start_time
        print("Runtime:", current_time)
        print("Finished before Timeout!")
    except multiprocessing.TimeoutError:
        current_time = time.perf_counter() - start_time
        print("Runtime:", current_time)
        pool.terminate()
        print("Timed out!")
        result = None
    print("Result:", result)
    print("Ended Timeout-Process")
    pool.close()


def main():
    # OLD Q-RES!!
    f = formats.Qdimacs(120, 120, 120)
    func_with_bench(qbf_solver.qbf_solve, [f], 5)
    print("End Timer")


if __name__ == "__main__":
    main()
