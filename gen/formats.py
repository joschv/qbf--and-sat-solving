import random as rand

import util.path as path_util


class Formula:
    nbvar = 0
    nbclauses = 0
    nbquant = 0
    clause_length = 0
    quant_set = [[]]
    clauses = [[]]

    def __init__(self, nbvar=8, nbclauses=4, nbquant=0, clause_length=0):
        if clause_length > nbvar:
            clause_length = nbvar
            print("Trying to create clause with greater length than number of variable! Set clause_length equal nbvar.")
        self.nbvar = nbvar
        self.nbclauses = nbclauses
        self.nbquant = nbquant
        self.quant_set = Qdimacs.generate_quant_set(nbvar, nbquant)
        self.clauses = self.generate_clauses(clause_length)
        self.clause_length = clause_length

    def clear(self):
        self.nbvar = 0
        self.nbclauses = 0
        self.nbquant = 0
        self.quant_set.clear()
        self.clauses.clear()

    def generate_clauses(self, length=0):
        self.clauses = [self.make_clause(self.nbvar, mode="new", length=length)]
        for i in range(1, self.nbclauses):
            self.clauses.append(self.make_clause(self.nbvar, mode="new", length=length))
        for c in self.clauses:
            if c == [0]:
                self.clauses.remove(c)
        return self.clauses

    def new_clause_gen(self):
        self.clauses = []
        for i in range(0, self.nbclauses):
            self.clauses.append([])

    @staticmethod
    def make_clause(nbvar, mode="old", length=0):
        if mode == "old":
            clause = []
            # for every variable
            for i in range(1, nbvar + 1):
                # random choice if positively, negatively or not included in clause
                x = rand.choice([i, 0, -i])
                if x != 0:
                    clause.append(x)
            # end clause with a 0 (convention)
            clause.append(0)
            return clause
        elif mode == "new":
            clause = []
            if length == 0:
                length = rand.choice(range(1, nbvar + 1))
            pool = list(range(1, nbvar + 1))
            for i in range(0, length):
                draw = rand.choice(pool)
                signum = rand.choice([-1, 1])
                pool.remove(draw)
                clause.append(signum * draw)
            clause.sort(key=lambda l: abs(l))
            clause.append(0)
            return clause

    def to_string(self):
        s = "Universal Formula\n"
        s += "No. Vars: " + str(self.nbvar) + "\n"
        s += "No. Clauses: " + str(self.nbclauses) + "\n"
        s += "No. Quant: " + str(self.nbquant) + "\n"
        s += "Quant sets: " + str(self.quant_set) + "\n"
        s += "Clauses: " + str(self.clauses) + "\n"
        return s


class Qdimacs(Formula):
    problem_or_solution = "p"
    form = "cnf"

    def __init__(self, nbvar=8, nbclauses=5, nbquant=5, clause_length=0):
        if nbquant > nbvar:
            print("Trying to create a formula with more quantors than variables! nbquant is set to nbvar.")
            nbquant = nbvar
        super().__init__(nbvar, nbclauses, nbquant, clause_length)
        self.set_header(nbvar, nbclauses)

    def clear(self):
        super().clear()
        self.set_header()

    def get_header_line(self):
        return self.problem_or_solution + " " + self.form + " " + str(self.nbvar) + " " + str(self.nbclauses)

    def set_header(self, nbvar=-1, nbclauses=-1, prob_or_solution="p", core_type="cnf"):
        self.nbvar = nbvar
        self.nbclauses = nbclauses
        self.problem_or_solution = prob_or_solution
        self.form = core_type

    @staticmethod
    def generate_quant_set(nbvar, nbquant):
        quant_set = []
        available = list(range(1, nbvar + 1))
        cur_q_set = []
        # remove free variables from available variables to be quantfied
        nb_of_free_vars = nbvar - nbquant
        for i in range(0, nb_of_free_vars):
            available.remove(rand.choice(available))
        # quantify remaining variables
        e_mode = False
        a_mode = False
        for i in range(0, nbquant):
            # dequeue
            cur_var = available[0]
            available.remove(cur_var)
            # choose quantifier and handle concurrent quantor choices
            if rand.choice(['e', 'a']) == 'e':
                if e_mode:
                    # collect multiple e-quantified variables in a row
                    cur_q_set.append(cur_var)
                else:
                    #   mode switched
                    if a_mode:
                        cur_q_set.append(0)
                        quant_set.append(cur_q_set)
                    cur_q_set = ['e', cur_var]
                e_mode = True
                a_mode = False
            else:
                if a_mode:
                    # collect
                    cur_q_set.append(cur_var)
                else:
                    # switch
                    if e_mode:
                        cur_q_set.append(0)
                        quant_set.append(cur_q_set)
                    cur_q_set = ['a', cur_var]
                e_mode = False
                a_mode = True
        cur_q_set.append(0)
        quant_set.append(cur_q_set)
        return quant_set

    def to_string(self):
        return "" + self.get_header_line() + "\n" + str(self.quant_set) + "\n" + str(self.clauses)

    def to_qcnf_file(self):
        s = self.get_header_line() + " \n"
        for qs in self.quant_set:
            if qs == [0]:
                continue
            for q in qs:
                s += str(q)
                s += " "
            s += "\n"
        for c in self.clauses:
            for x in c:
                s += str(x) + " "
            s += "\n"
        return s

    def get_formula(self):
        qcnf_formula = [self.get_header_line()]
        if self.quant_set:
            for q in self.quant_set:
                qcnf_formula.append(q)
        for c in self.clauses:
            qcnf_formula.append(c)
        return qcnf_formula

    def write_qdimacs_file(self, file_path, f_id):
        fl = open(file_path + str(f_id) + ".qcnf", "w+")
        fl.write(self.to_qcnf_file())
        fl.close()

    def import_file(self, file_path):
        """
        Imports any file that follows the QDIMACS standards and assigns all variables of the Qdimacs instance to the
        values of the formula specified in the file. Also converts all numbers written in the file to Integers in the
        instance. :param file_path: The path to the file that will be opened. Has to be created according to QDIMACS
        standards. :return: Return True if successful.
        """
        header = []
        self.clear()
        fl = open(file_path, "r")
        if not fl:
            return False
        lines = fl.readlines()
        for line in lines:
            split_line = line.split(" ")
            if len(split_line) == 0:
                continue
            if "\n" in split_line:
                split_line.remove("\n")
            if split_line[0] == "c":
                # comment line
                continue
            elif split_line[0] == "p":
                # header line
                for i in range(0, 2):
                    header.append(split_line[i])
                for i in range(2, 4):
                    header.append(int(split_line[i]))
                self.set_header(header[2], header[3], header[0], header[1])
            elif split_line[0] == "a" or split_line[0] == "e":
                # quant line
                q_line = [split_line[0]]
                for i in range(1, len(split_line)):
                    q_line.append(int(split_line[i]))
                self.quant_set.append(q_line)
            else:
                # clause line
                c_line = []
                for i in range(0, len(split_line)):
                    c_line.append(int(split_line[i]))
                if 0 in c_line:
                    c_line.remove(0)
                c_line.sort(key=lambda l: abs(l))
                c_line.append(0)
                self.clauses.append(c_line)
        fl.close()

        return True

    @staticmethod
    def generate_qdimacs_files(nbfiles, nbvar, nbclauses, nbquant, clause_length=0):
        for i in range(0, nbfiles):
            Qdimacs(nbvar, nbclauses, nbquant, clause_length).write_qdimacs_file(
                str(path_util.get_project_src()) + "/files/qdimacs/qdimacs_", str(i))

    def to_dimacs(self):
        d = Dimacs()
        d.nbvar = self.nbvar
        d.nbclauses = self.nbclauses
        d.clauses = self.clauses
        d.clause_length = self.clause_length
        return d


class Dimacs(Qdimacs):
    # every instance of DIMACS is also an instance of QDIMACS

    def __init__(self, nbvar=8, nbclauses=5, clause_length=0):
        super().__init__(nbvar, nbclauses, 0, clause_length)
        self.quant_set.clear()

    def write_dimacs_file(self, file_path, f_id):
        f = open(file_path + str(f_id) + ".cnf", "w+")
        f.write(self.to_cnf_file())

    @staticmethod
    def generate_dimacs_files(nbfiles, nbvar, nbclauses, clause_length=0):
        for i in range(0, nbfiles):
            Dimacs(nbvar, nbclauses, clause_length).write_dimacs_file(str(path_util.get_project_src()) +
                                                                      "/files/dimacs/dimacs_", str(i))

    def get_formula(self):
        qcnf_formula = self.header
        for c in self.clauses:
            qcnf_formula.append(c)
        return qcnf_formula

    def to_cnf_file(self):
        s = self.get_header_line() + " \n"
        for c in self.clauses:
            for x in c:
                s += str(x) + " "
            s += "\n"
        return s

    def to_string(self):
        return "" + self.get_header_line() + "\n" + str(self.clauses)


if __name__ == "__main__":
    Dimacs.generate_dimacs_files(20, 25, 20)
    # Dimacs.generate_dimacs_files(1, 5, 4)
    # Qdimacs.generate_qdimacs_files(20, 25, 20, 25)
    # Qdimacs.generate_qdimacs_files(1, 5, 4, 5)
