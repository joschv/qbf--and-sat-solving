from algorithms.qresolution2 import q_solve
from gen.formats import Qdimacs
from util.path import get_project_path


def q_solve_test():
    no_tests = 5
    solutions = [True, False, False, False, True]
    test_file_string = get_project_path() + "/deploytests/files/q_"
    for i in range(0, no_tests):
        print("\n----- ----- ----- -----")
        print("Test No. " + str(i))
        print("----- ----- ----- -----")
        f = Qdimacs()
        f.import_file(test_file_string + str(i) + ".qcnf")
        s = q_solve.Q_Solve(f)
        result = s.solve_q_res()
        if result != solutions[i]:
            print("Test failed: ", i, "!")
            return False
    print("All tests passed!")
    return True
