from gen.formats import Formula, Dimacs, Qdimacs
from util.path import get_project_path


def formats_tests():
    print("Test: Formula")
    f = Formula(6, 4, 6)
    print(f.to_string())

    print("Test: Qdimacs")
    q = Qdimacs(6, 4, 5)
    print(q.to_string())
    print("Assert: Header\n", q.get_header_line())
    print("Assert: Quantor set\n", q.quant_set, "\n")

    print("Test: Write/Read Qdimacs file")
    q.write_qdimacs_file(get_project_path() + "/files/temp/qdimacs_", 0)
    q2 = Qdimacs()
    q2.import_file(get_project_path() + "/files/temp/qdimacs_0.qcnf")
    print(q2.to_string())
    print("Assert: Header\n", q2.get_header_line())
    print("Assert: Quantor set\n", q2.quant_set, "\n")

    print("Test: Dimacs")
    d = Dimacs(6, 4)
    print(d.to_string())
    print("Assert: Header\n", d.get_header_line())
    print("Assert: Quantor set\n", d.quant_set, "\n")

    print("Test: Write/Read Dimacs file")
    d.write_dimacs_file(get_project_path() + "/files/temp/dimacs_formula_", 0)
    d2 = Dimacs()
    d2.import_file(get_project_path() + "/files/temp/dimacs_formula_0.cnf")
    print(d2.to_string())
    print("Assert: Header\n", d2.get_header_line())
    print("Assert: Quantor set\n", d2.quant_set, "\n")

    return True
