import util.path as util_path
from algorithms.qreduction import qbf_naive
from algorithms.resolution import sat_solver
from benchmarking.timer import func_with_bench
from gen import formats


def test(n):
    for i in range(0, n):
        formula = formats.Dimacs()
        assert sat_solver.cnf_solve(formula) == qbf_naive.qbf_naive(formula)
    return True


def a_test():
    formula_1 = formats.Dimacs()
    formula_1.import_file(str(util_path.get_project_src()) + "/files/dimacs/dimacs_0.cnf")
    func_with_bench(sat_solver.cnf_solve, [formula_1], 20)
    func_with_bench(qbf_naive.qbf_naive, [formula_1], 20)


if __name__ == "__main__":
    test(100)
