# QBF- & SAT-Solving

Repository corresponding to bachelor thesis project "Algorithms for quantified Boolean Formulas".

---

#### 1. DIMACS implementation & generation [07.01.20]

#### 2. Davis-Putnam algorithm (SAT-Solving) [08.01.20]

#### 3. QDIMACS implementation & generation [13.01.20]

#### 4. DIMACS/QDIMACS file export [25.01.20]

#### 5. Q-Resolution (QBF-Solving) [<21.04.20]

#### 6. QBF-Reduction [<21.04.20]

#### 7. Cryptosat calling (SAT-Solving) [<21.04.20]

#### 8. Timing function calls [<21.04.20]

#### 9. Collect runtime data of algorithms [21.04.20]

#### 10. Collect memory usage data of algorithms [22.04.20] 

#### 11. File logs of time and memory data [23.04.20]

#### 12. Benchmark series [30.04.20]

#### 13. CI/CD [<23.05.20]

#### 14. Davis-Putnam-Logemann-Loveland algorithm (SAT-Solving) [06.07.20]