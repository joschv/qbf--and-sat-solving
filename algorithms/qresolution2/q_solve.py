from enum import Enum

import util.path as path_util
from algorithms.qresolution.qbf_solver import is_a_literal_of
from gen.formats import Qdimacs


class Quantor(Enum):
    """
    Realises quantors with an enumeration. There are 2 possibilities for the Quantor:
    FOR_ALL==0 and EXISTS==1.
    """
    FOR_ALL = 0
    EXISTS = 1


class Q_Literal:
    """A Q_Literal is a literal that contains information about its quantification.

    A Q_Literal is created with and contains 2 attributes. The literal and its quantifying quantor.
    The literal is basically just an integer where 1 stands for the first quantified literal in the prefix, 2 for the
    second etc. The quantor is realised with an enumeration. There are 2 possibilities for the Quantor:
    FOR_ALL and EXISTS.
    Special occurences of Q_Literals are "Quantor.EXISTS, 0" which stands for "Tautology" and "Quantor.FOR_ALL, 0" which
    stands for "False/Empty clause".
    Attributes:
        quantor: A Quantor(Enum) that quantifies the literal.
        literal: An Integer. Negative values stand for negated literals.
    """
    quantor = Quantor.EXISTS
    literal = 0

    # FOR_ALL 0 stands for EMPTY_CLAUSE
    # EXISTS 0 stands for TAUTOLOGY

    def __init__(self, quantor=Quantor.EXISTS, literal=0):
        """
        The constructor method. Takes a Quantor and an integer.
        :param quantor: A Quantor.
        :param literal: An integer. Special case 0. See Q_Literal description.
        """
        if quantor not in [Quantor.EXISTS, Quantor.FOR_ALL]:
            print("Invalid quantor input!")
            return
        self.quantor = quantor
        self.literal = literal

    def to_string(self):
        """
        Returns a string containing a "V" for a FOR_ALL quantified literal, "E" for a EXISTS quantified one, as well as
        the literal.
        :return: The constructed string.
        """
        if self.quantor == Quantor.FOR_ALL:
            quantor_string = "V"
        else:
            quantor_string = "E"
        return quantor_string + " " + str(self.literal)

    def equal(self, q_literal):
        """
        Returns True if the given Q_Literal is equal to itself, meaning it is the same Quantor and Literal.
        :param q_literal: The Q_Literal that is compared to.
        :return: A boolean value, True if equal, False otherwise.
        """
        if self.quantor != q_literal.quantor:
            return False
        if self.literal != q_literal.literal:
            return False
        return True


class Q_Clause:
    """A Q_Clause contains a list of Q_Literals.

    A Q_Clause if a list of Q_Literals. Q_Literals contain information of the literals quantification which
    makes quick evaluation of the Q_Clause possible - no referring to the quantor prefix needed.
    Attributes:
        q_literals: The list of Q_Literals.
    """
    q_literals = []

    def __init__(self):
        self.q_literals = []

    def add_q_literal(self, new_q_literal=Q_Literal()):
        """
        Adds a Q_Literal to the Q_Clause. Does not add duplicates. If the new Q_Literal makes the Q_Clause a tautology,
        the Q_Clause is emptied and False is returned.
        :param new_q_literal: The Q_Literal that must be added to the Q_Clause.
        :return: Returns True if new_q_literal was added to the Q_Clause or was already contained in it. Returns False
        if the addition of new_q_literal makes the Q_Clause a tautology.
        """
        new_val = abs(new_q_literal.literal)
        for i in range(len(self.q_literals)):
            q_literal = self.q_literals[i]
            old_val = abs(q_literal.literal)
            if new_val == old_val:
                # x == x or x == -x
                if new_q_literal.literal == q_literal.literal:
                    # literal already contained
                    assert q_literal.quantor == new_q_literal.quantor
                    return True
                assert new_q_literal.literal == -q_literal.literal
                # x and -x in clause -> tautology# clause is now tautological
                # print("Tautology", q_literal.to_string(), new_q_literal.to_string())
                self.q_literals = [Q_Literal()]
                return False
            if new_val > old_val:
                # insert sorted
                self.q_literals.insert(i, new_q_literal)
                return True
        # add to end of list
        self.q_literals.append(new_q_literal)
        return True

    def remove_literal(self, literal, check=False):
        """
        Removes the given Q_Literal and checks if the Q_Clause becomes a FOR_ALL-Clause.
        :param literal: The Q_Literal that must be removed.
        :return: Returns True if the Q_Literal is removed and the Q_Clause did not become FOR_ALL-Clause or empty.
        """
        found = False
        for q_literal in self.q_literals:
            if literal == q_literal.literal:
                found = q_literal
                break
        if not found:
            return True
        else:
            self.q_literals.remove(found)
            return not check and not self.is_for_all_clause()

    def combine(self, q_clause_1, q_clause_2):
        """
        Combines two Q_Clauses to create a third. This Q_Clause is the third, the new Q_Clause.
        :param q_clause_1: First Q_Clause that must be combined.
        :param q_clause_2: Second Q_Clause that must be combined.
        :return: Returns False if the resulting Q_Clause is a tautology. Returns True if the Q_Clause is
        combined successfully and not a tautology.
        """
        for q_clause in q_clause_1, q_clause_2:
            for q_literal in q_clause.q_literals:
                if not self.add_q_literal(q_literal):
                    return False
        return True

    def is_for_all_clause(self):
        """
        Checks if the Q_Clause is a FOR_ALL-Clause and thereby False.
        :return: Returns True if the clause is a FOR_ALL-Clause or empty. Returns False otherwise.
        """
        for q_literal in self.q_literals:
            if q_literal.quantor != Quantor.FOR_ALL:
                return False
        # print("FORALL CLAUSE:", self.to_string())
        return True

    def is_empty(self):
        """
        Checks if the Q_Clause is empty.
        :return: Returns True if the list of Q_Literals is empty.
        """
        return len(self.q_literals) == 0

    def is_tautology(self):
        """
        Checks if the Q_Clause is a tautology indicated by the Q_Literal (Quantor.EXISTS, 0).
        :return: Returns True if length is 1 and the only Q_Literal is the tautology indicator.
        """
        if len(self.q_literals) == 1:
            return self.q_literals[0].quantor == Quantor.EXISTS and self.q_literals[0].literal == 0
        return False

    def sort(self):
        """
        Sorts the Q_Literals of the Q_Clause by the number of the literal, disregarding the "-" for negation.
        """
        self.q_literals.sort(key=lambda x: abs(x.literal))

    def contains(self, literal):
        """
        Iterates over the Q_Literals and searches for the literal of the parameter.
        :param literal: An integer that represents the literal searched for.
        :return: Returns True if the literal is part of the Q_Clause. Returns False otherwise.
        """
        for q_literal in self.q_literals:
            if literal == q_literal.literal:
                return True
        return False

    def to_string(self):
        """
        Builds a string containing all Q_Literals of the Q_Clause surrounded by parentheses.
        :return: Returns the constructed string.
        """
        string = "("
        for q_literal in self.q_literals:
            string += q_literal.to_string()
            string += ", "
        string = string.rsplit(",", 1)[0]
        string += ")\n"
        return string

    def equal(self, q_clause):
        if len(self.q_literals) != len(q_clause.q_literals):
            return False
        for i in range(0, len(self.q_literals)):
            if not self.q_literals[i].equal(q_clause.q_literals[i]):
                return False
        return True


class Q_Solve:
    """A Q_Solve instance contains a Qdimacs formula and is intended for solving it.

    A Q_Solve instance contains a list of Q_Clauses that is created from a Qdimacs formula on construction.
    It can be used to solve QBF problems by Q-Resolution.

    Attributes:
        q_clauses: A list of Q_Clauses.
        solution: A boolean containing None until a solution is found. The solution is either True or False.
    """
    q_clauses = []
    nbvar = None
    frequencies = []
    heuristic = []
    final_result = False
    solution = None
    certificate = None
    certificate_source = None

    def __init__(self, problem=Qdimacs()):
        """
        Creates a new instance of Q_Solve from a Qdimacs formula.
        :param problem: The Qdimacs formula that describes the QBF problem.
        """
        self.nbvar = problem.nbvar
        self.final_result = False
        self.solution = None
        self.certificate = "Believe me.."
        self.certificate_source = "__init__"
        # iterate over clauses and change literals to Q_Literals
        self.q_clauses = []
        for clause in problem.clauses:
            q_clause = Q_Clause()
            for literal in clause:
                if literal == 0:
                    continue
                if is_a_literal_of(literal, problem.quant_set) >= 0:
                    q_literal = Q_Literal(Quantor.FOR_ALL, literal)
                else:
                    q_literal = Q_Literal(Quantor.EXISTS, literal)
                if not q_clause.add_q_literal(q_literal):
                    break
            q_clause.sort()
            if not self.add_q_clause(q_clause):
                # a FOR_ALL-Clause was added, Q-Formula certainly False
                self.set_solution(False, "Inconsistent clause in formula:" + q_clause.to_string(), "__init__")

    def to_string(self):
        """
        Builds a string containing the Q_Solve instance's information down to the Q_Literals.
        :return: The constructed string.
        """
        string = "Q-Solve Problem:\n"
        for q_clause in self.q_clauses:
            string += q_clause.to_string()
        return string

    def add_q_clause(self, q_clause):
        """
        Adds a Q_Clause to the q_clauses list of Q_Solve. Does sort the Q_Clause before addition. Does not add duplicates.
        Checks if the Q_Clause is a FOR_ALL-Clause and thereby False and returns False if it is the case.
        :param q_clause: The Q_Clause that must be added to the Q_Solve's q_clauses list.
        :return: Returns False if the Q_Clause is False and falsifies the Q-Formula. Returns True otherwise.
        """
        q_clause.sort()
        if q_clause in self.q_clauses:  # does this work???
            pass
        elif q_clause.is_for_all_clause():
            self.q_clauses.append(q_clause)
            return False
        else:
            self.q_clauses.append(q_clause)
            return True

    def remove_q_clause(self, q_clause):
        q_clause.sort()
        if q_clause in self.q_clauses:  # does this work???
            self.q_clauses.remove(q_clause)

    def solve_q_res(self):
        print("\nQ-Solve for:")
        print(self.to_string())
        # check clauses of formula for inconsistency
        for q_clause in self.q_clauses:
            if q_clause.is_for_all_clause():
                self.set_solution(False, "Inconsistent clause in formula: " + q_clause.to_string(), "solve_q_res")
                break
        if not self.final_result:
            # start q-res
            self.get_frequency()
            for i in reversed(self.get_heuristic()):
                if self.q_resolution(i[0]):
                    break
        if not self.final_result:
            self.set_solution(True, "Could not show inconsistency by Q-Resolution.", "solve_q_res")
        self.print_result()
        return self.solution

    def solve_q_res_alternative(self):
        print("\nAlternative Q-Solve for:")
        print(self.to_string())
        # check clauses of formula for inconsistency
        for q_clause in self.q_clauses:
            if q_clause.is_for_all_clause():
                self.set_solution(False, "Inconsistent clause in formula: " + q_clause.to_string(), "solve_q_res")
                break
        if not self.final_result:
            # start q-res
            self.get_frequency()
            for i in self.get_heuristic():
                if self.q_resolution(i[0]):
                    break
        if not self.final_result:
            self.set_solution(True, "Could not show inconsistency by Q-Resolution.", "solve_q_res")
        self.print_result()
        return self.solution

    def q_resolution(self, i):
        """

        :param i:
        :return: True if the q_resolution delivers a final result.
        """
        print("----- ----- ----- -----")
        print("Resolve:", i)
        print(self.to_string())
        positives = []
        negatives = []
        for q_clause in self.q_clauses:
            if q_clause.contains(i):
                positives.append(q_clause)
            elif q_clause.contains(-i):
                negatives.append(q_clause)

        resolvents = []
        remove = []
        for qc_1 in positives:
            for qc_2 in negatives:
                resolvent = self.q_resolve(i, qc_1, qc_2)
                is_tautology = resolvent.is_tautology()
                is_inconsistent = resolvent.is_for_all_clause()
                if is_inconsistent:
                    self.set_solution(False, "Inconsistent resolvent:" + qc_1.to_string() + " + " + qc_2.to_string() +
                                      " |- " + resolvent.to_string(), "q_resolution")
                elif is_tautology:
                    qc_1.add_q_literal(Q_Literal(Quantor.EXISTS, i))
                    qc_2.add_q_literal(Q_Literal(Quantor.EXISTS, -i))
                    continue
                else:
                    duplicate = False
                    for q_clause in resolvents:
                        if resolvent.equal(q_clause):
                            duplicate = True
                    if not duplicate:
                        remove.append(qc_1)
                        remove.append(qc_2)
                        resolvents.append(resolvent)
                    else:
                        qc_1.add_q_literal(Q_Literal(Quantor.EXISTS, i))
                        qc_2.add_q_literal(Q_Literal(Quantor.EXISTS, -i))

        print("Resolvents:")
        for q_clause in resolvents:
            print(q_clause.to_string())

        for q_clause in remove:
            self.remove_q_clause(q_clause)

        for q_clause in resolvents:
            self.add_q_clause(q_clause)

        print("Resolved:", self.to_string())
        print("Solved?", self.final_result)
        print("----- ----- ----- -----")
        return self.final_result

    @staticmethod
    def q_resolve(i, q_clause_1, q_clause_2):
        qc_1 = q_clause_1
        qc_2 = q_clause_2

        # step 1: elimination of FOR_ALL-Literals from the end
        for qc in qc_1, qc_2:
            remove = []
            for q_literal in reversed(qc.q_literals):
                if q_literal.quantor == Quantor.FOR_ALL:
                    remove.append(q_literal)
                else:
                    break
            for q_literal in remove:
                qc.remove_literal(q_literal)
            # NOTE: can not be emptied, because qresolution is done on EXISTS-Literals

        # step 2: eliminate i and -i
        qc_1.remove_literal(i)
        qc_2.remove_literal(-i)

        # step 3: combine qc_1 and qc_2
        resolvent = Q_Clause()
        resolvent.combine(qc_1, qc_2)
        resolvent.sort()
        return resolvent

    def get_frequency(self):
        """
        Sums occurences of literals and negated literals and saves the sum in a list under the index of the literal.
        """
        self.frequencies = []
        if not self.nbvar:
            self.find_nbvar()
        for i in range(0, self.nbvar * 2 + 1):
            self.frequencies.append(0)
        self.frequencies[0] = self.nbvar
        for q_clause in self.q_clauses:
            for q_literal in q_clause.q_literals:
                if q_literal.quantor == Quantor.FOR_ALL:
                    continue
                else:
                    self.frequencies[q_literal.literal] += 1
        return self.frequencies

    def get_heuristic(self):
        """
        Draws information from the frequencies list and collects EXISTS-literals that occur positively and negated.
        Creates a list that is sorted by most frequent occuring literals.
        :return: Returns the heuristic list of tuples.
        """
        if not self.nbvar or not self.frequencies:
            return
        self.heuristic = []
        for i in range(1, self.nbvar + 1):
            pos = self.frequencies[i]
            neg = self.frequencies[-i]
            if not pos or not neg:
                continue
            frequency = self.frequencies[i] + self.frequencies[-i]
            pair = (i, frequency)
            self.heuristic.append(pair)
        self.heuristic.sort(key=lambda x: x[1], reverse=True)
        return self.heuristic

    def set_solution(self, result, certificate="Believe me!", certificate_source="Not given."):
        self.final_result = True
        self.solution = result
        self.certificate = certificate
        self.certificate_source = certificate_source

    def print_result(self):
        report = "Q-Resolution for:\n" + self.to_string()
        result = "Result: " + str(self.solution)
        certificate = "Certificate: " + self.certificate
        source = "(Certificate by: " + self.certificate_source + ")"
        print(report)
        print(result)
        print(certificate)
        print(source)


def main():
    formula = Qdimacs(26, 48, 26)
    formula.import_file(str(path_util.get_project_src()) + "/files/dimacs/dimacs_0.cnf")
    print(formula.to_string(), "\n")
    sol = Q_Solve(formula)
    print(sol.to_string())
    print(sol.solution)
    sol.solve_q_res()


if __name__ == "__main__":
    main()
