from subprocess import PIPE, run

from gen.formats import Dimacs


def out(command):
    result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True, shell=True)
    return result.stdout


def write_file(formula=Dimacs(), file_name="temp", file_id=0):
    formula.to_dimacs().write_dimacs_file(file_name, file_id)


def run_crypto_sat(file_name, file_id):
    result = out('powershell.exe cat ' + str(file_name) + str(file_id) + ".cnf" +
                 ' | docker run --rm -i msoos/cryptominisat')
    val, time, mem = get_data(result)
    return val, time, mem


def get_satifiability(file):
    result = out('powershell.exe cat ' + str(file) + ' | docker run --rm -i msoos/cryptominisat')
    val, time, mem = get_data(result)
    return val


def get_data(output=""):
    print(output)
    lines = output.split("\n")
    result = time = mem = None
    for line in lines:
        if line.startswith("c Mem used"):
            mem = int(float(line.split()[-2]) * pow(10, 6))
        elif line.startswith("c Total time"):
            temp = line.split(" ")
            time = float(temp[6])
        elif line.startswith("s "):
            result = line.split()
            if result[1] == 'UNSATISFIABLE':
                result = False
            elif result[1] == 'SATISFIABLE':
                result = True
            else:
                result = "ERROR"
    print("Result:", result)
    print("Time:", time)
    return result, time, mem


if __name__ == "__main__":
    f = Dimacs(10, 40, 3)
    write_file(f, "temp_", 0)
    run_crypto_sat("temp_", 0)
