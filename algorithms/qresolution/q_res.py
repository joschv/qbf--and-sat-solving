import algorithms.qresolution.qbf_solver as qbf_solver
import algorithms.resolution.sat_solver as sat_solver
import gen.formats as formats
import util.path as path_util


def find_resolvable_literal(qdimacs_formula, lit):
    if lit < 1:
        return None

    if qbf_solver.is_a_literal_of(lit, qdimacs_formula.quant_set) > 0:
        find_resolvable_literal(qdimacs_formula, lit - 1)

    result = check_resolvable(qdimacs_formula, lit)
    if not result:
        return find_resolvable_literal(qdimacs_formula, lit - 1)
    else:
        return lit, result[0], result[1]


def check_resolvable(qdimacs_formula, literal):
    # for every variable
    positive = []
    negative = []

    for c in qdimacs_formula.clauses:
        # check every clause for positive or negative occurence
        if literal in c:
            positive.append(c)
        elif -literal in c:
            negative.append(c)

    if len(positive) > 0 and len(negative) > 0:
        return positive, negative
    else:
        return None


def eliminate_late_a_literals(qdimacs_formula, clause):
    # implementation of step 1 of the Q-Resolution algorithm by Kleine Buening
    for literal in reversed(clause):
        if literal == 0:
            continue
        index = qbf_solver.is_a_literal_of(literal, qdimacs_formula.quant_set)
        if index < 0:
            # WIE VERHÄLT SICH SCHRITT 2 DER Q-RESOLUTION IN DEM FALL WO VOR EINEM A-LITERAL NUR EINE FREIE VARIABLE
            # STEHT? REGIERT DIESE WIE EIN E-LITERAL DAS A-LITERAL, ICH GLAUBE NICHT, DAHER HIER VORERST ABFRAGE OB
            # E-LITERAL -> DANN ABBRUCH. NEVERMIND, NACH BUCH SIND FREIE UND E-GEBUNDENE LITERALE E-LITERALE
            # if QBF_Solver.is_e_literal_of(literal, qdimacs_formula):
            assert clause != [0]
            return clause, False
        else:
            clause.remove(literal)
    return clause, True


def resolve(qdimacs_formula, i, positive_occurence, negative_occurence):
    print("Resolve with", i, "..")
    # step 1: eliminate all occurences of forall literals that are not smaller than a exists literal in the same clause
    for clause in positive_occurence:
        elim_clause, is_taut = eliminate_late_a_literals(qdimacs_formula, clause)
        if is_taut or i not in elim_clause:
            positive_occurence.remove(clause)
        else:
            clause = elim_clause
    for clause in negative_occurence:
        elim_clause, is_taut = eliminate_late_a_literals(qdimacs_formula, clause)
        if is_taut or -i not in elim_clause:
            negative_occurence.remove(clause)
        else:
            clause = elim_clause
    # if all parent clauses were eliminated in step 1, skip the rest
    if len(positive_occurence) == 0 and len(negative_occurence) == 0:
        print("Step 1 Elimination.")
        return False
    else:
        # step 2: eliminate all positive or negative occurences of literal i in the clauses containing it
        parents = []
        for pos in positive_occurence:
            for neg in negative_occurence:
                # combine clauses
                resolvent = pos + neg
                # remove occurences
                for rm in [0, i, -i]:
                    resolvent.remove(rm)
                if resolvent == [0]:
                    print(pos, neg, "|-", [], "Inconsistent!")
                    qdimacs_formula.clauses = []
                    return True
                assert resolvent != []
                resolvent = sat_solver.clean_up_clause(qdimacs_formula.nbvar, resolvent)
                # if a non-tautologic, non-duplicate, add it to the resolvent list
                if resolvent not in qdimacs_formula.clauses and resolvent != "taut":
                    if qbf_solver.is_a_clause(resolvent, qdimacs_formula.quant_set):
                        print(pos, neg, "\n|-", resolvent, "\n|-", [], "Inconsistent!")
                        qdimacs_formula.clauses = []
                        return True
                    qdimacs_formula.clauses.append(resolvent)
                # add the parent clauses to the list of to be removed clauses
                parents.append(pos)
                parents.append(neg)
        # remove clauses that produced a resolvent
        for clause in parents:
            if parents in qdimacs_formula.clauses:
                qdimacs_formula.clauses.remove(clause)
    print("Resolved with", i, "!\n", qdimacs_formula.to_string())
    return False


def q_res(qdimacs_formula=formats.Qdimacs()):
    # implementation of the q-qresolution described in the book 'propositional logic: deduction and algorithms'
    # by Hans kleine buening and theodor lettmann
    print("Q-Resolution:")
    resolvable = find_resolvable_literal(qdimacs_formula, qdimacs_formula.nbvar + 1)
    finished = False
    while resolvable:
        finished = resolve(qdimacs_formula, resolvable[0], resolvable[1], resolvable[2])
        if finished:
            break
        if resolvable[0] < 1 or len(resolvable[1]) < 1 or len(resolvable[2]) < 1:
            print(resolvable)
        resolvable = find_resolvable_literal(qdimacs_formula, resolvable[0] - 1)
    if finished or not resolvable:
        if qdimacs_formula.clauses:
            for c in qdimacs_formula.clauses:
                if qbf_solver.is_a_clause(c, qdimacs_formula.quant_set):
                    break
            return True
    return False


def main():
    print("Testing Q-Resolution by Kleine Buening")
    formula = formats.Qdimacs()
    formula.import_file(path_util.get_project_path() + "/files/dimacs/dimacs_0.cnf")
    print(formula.to_string())
    res = q_res(formula)
    print(res)


if __name__ == "__main__":
    main()
