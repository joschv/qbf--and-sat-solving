import random as rand

import algorithms.qreduction.qbf_naive as qbf_naive
import algorithms.qresolution.q_res as q_res
from benchmarking import timer
from gen.formats import Qdimacs


def is_a_literal_of(x, quant_set):
    if x < 0:
        x = -x
    for q in range(0, len(quant_set)):
        if x in quant_set[q]:
            if quant_set[q][0] == 'a':
                return q
            else:
                return -1
    return -1


def is_e_literal_of(x, quant_set):
    assert x > 0
    for q in range(0, len(quant_set)):
        if x in quant_set[q]:
            if quant_set[q][0] == 'e':
                return q
            else:
                return -1
    return -1


def is_a_clause(clause, quant_set):
    for lit in clause:
        if lit == 0:
            continue
        if is_e_literal_of(abs(lit), quant_set) >= 0:
            return False
    return True


def qbf_solve(qdimacs_formula=Qdimacs(), resolver='Q_Res'):
    print("Solve QBF:")
    print(qdimacs_formula.to_string())
    if resolver == "Q_Res":
        result = q_res.q_res(qdimacs_formula)
    if resolver == "naive":
        result = qbf_naive.qbf_naive(qdimacs_formula)
    if result:
        qdimacs_formula.set_header(qdimacs_formula.nbvar, qdimacs_formula.nbclauses, "s")
        print("Q-Res Solution:\n", qdimacs_formula.to_string())
    else:
        print("Q-Res Problem:\n", qdimacs_formula.to_string())
    return result


def test_is_lit(n):
    for i in range(0, n):
        formula = Qdimacs()
        print(formula.to_string())
        c = rand.choice(range(0, formula.nbvar))
        print(c)
        print("A-Literal", is_a_literal_of(c, formula.quant_set))
        print("E-Literal", is_e_literal_of(c, formula.quant_set))


def test_qres(n):
    for i in range(0, n):
        nbvar = rand.choice(range(10, 220))
        q_res_with_timeout(Qdimacs(nbvar, 1500, 1000))


def q_res_with_timeout(qdimacs_formula=Qdimacs(), timeout=20):
    return timer.func_with_timeout(qbf_solve, [qdimacs_formula], timeout)


def main():
    # test_is_lit(20)
    # test_qres(10)
    formula = Qdimacs(12, 12, 12)
    print(q_res_with_timeout(formula))
    # qbf_solve(qdimacs_formula=formula, resolver='Q_Res')


if __name__ == "__main__":
    main()
