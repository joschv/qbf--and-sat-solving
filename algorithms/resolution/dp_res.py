import algorithms.resolution.res_calc as res_calc


def dp_res(nbvar, nbclause, clauses):
    # Implementation of the Davis-Putnam Resolution
    print("Davis-Putnam Resolution:")
    # for every variable
    for i in range(1, nbvar + 1):
        # collect all clauses that contain a positive occurrence in p and negative occurrences in n (without the
        # occurrence itself)
        positive_occ_of_i = []
        negative_occ_of_i = []
        for c in clauses:
            if i in c:
                positive_occ_of_i.append(c)
            elif -i in c:
                negative_occ_of_i.append(c)
        # if there are positive and negative occurrences
        if not len(positive_occ_of_i) == 0 and not len(negative_occ_of_i) == 0:
            print("Resolve with", i, "...")
            resolvents = []
            parents = []
            # for every clause with a positive occurrence
            for p1 in positive_occ_of_i:
                # for every clause with a negative occurrence
                for n1 in negative_occ_of_i:
                    r1 = res_calc.resolve(i, set(p1), set(n1))
                    if r1 != "taut":
                        # check if inconsistency is proven
                        if r1 == 'false':
                            print(p1, n1, "|-", [], "Inconsistent!")
                            return False
                        r1 = list(r1)
                        r1.sort(key=lambda l: abs(l))
                        r1.append(0)
                        # the resulting clause is added to the result list
                        if r1 not in resolvents:
                            resolvents.append(r1)
            # the clauses which are used for qresolution are removed from the formula
            for c in positive_occ_of_i:
                clauses.remove(c)
            for c in negative_occ_of_i:
                clauses.remove(c)
            # and replaced with the clauses produced by qresolution
            clauses += resolvents
            print(clauses)
    solution = []
    # build solution from clauses
    for c in clauses:
        if c not in solution:
            solution.append(c)
    print("Solvable: ", solution)
    return True
