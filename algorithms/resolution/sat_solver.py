import algorithms.resolution.dp_res as dp_res
import gen.formats as formats


def read_header(header):
    assert header[0] == "p"
    assert header[1] == "cnf"
    nbvar: int = header[2]
    nbclause: int = header[3]
    assert len(header) == 4
    print("Header read: Number of variables is " + str(nbvar) + ", number of clauses is " + str(nbclause) + ".")
    return nbvar, nbclause


def clean_up_clause(nbvar, clause):
    zero_removed = False
    if 0 in clause:
        clause.remove(0)
        zero_removed = True
    # clean the clause up
    for i in range(1, nbvar + 1):
        # x or x <=> x
        if clause.count(i) > 1:
            clause.remove(i)
        if clause.count(-i) > 1:
            clause.remove(-i)
        # x or notx <=> 1
        if i in clause and -i in clause:
            return "taut"
    clause.sort(key=lambda x: abs(x))
    if zero_removed:
        clause.append(0)
    return clause


def remove_from_clause(rm, clause):
    for i in rm:
        for k in range(0, clause.count(i)):
            clause.remove(i)
    return clause


def cnf_solve(cnf_formula=formats.Dimacs(), resolver='dp'):
    nbvar, nbclause = cnf_formula.nbvar, cnf_formula.nbclauses
    if resolver == 'dp':
        return dp_res.dp_res(nbvar, nbclause, cnf_formula.clauses)
    elif resolver == 'mini':
        # implement MiniSAT call
        print("uff")


def main():
    formula = formats.Dimacs(5, 15)
    print(formula.to_string())
    cnf_solve(formula, 'dp')


if __name__ == "__main__":
    main()
