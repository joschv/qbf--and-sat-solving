from gen.formats import Dimacs

UNSAT = "UNSATISFIABLE"


def solve_dpll(formula=Dimacs()):
    clauses = formula.clauses
    for c in clauses:
        c.remove(0)
    assignment = dict()
    return dpll(clauses=clauses, assignment=assignment)


def dpll(clauses, assignment):
    # Implementation of the Davis/Putnam/Logemann/Loveland-Algorithm
    # Check for empty clause
    # IF EMPTY_CLAUSE THEN UNSAT
    if [] in clauses:
        return UNSAT
    # Check if no clauses left
    # Explanation: By assigning variables clauses can be fulfilled and disappear through simplification
    if not clauses:
        return assignment
    # Unit Propagation
    # Search for Unit Clause and assign variable such that the clause is fulfilled
    for c in clauses:
        if len(c) == 1:
            var = c[0]
            if var > 0:
                assignment[abs(var)] = True
            elif var < 0:
                assignment[abs(var)] = False
            clauses = simplify(clauses, abs(var), assignment[abs(var)])
            return dpll(clauses, assignment)
    # Splitting Rule
    # Select a variable
    # TODO: Choice rule
    # get first variable of first clause
    c = clauses[0]
    var = c[0]
    for d in [False, True]:
        clauses = simplify(clauses, abs(var), d)
        new_assignment = assignment.copy()
        new_assignment[abs(var)] = d
        new_assignment = dpll(clauses, new_assignment)
        if new_assignment != UNSAT:
            return new_assignment
    return UNSAT


def simplify(clauses, variable, value):
    assert variable > 0
    new_clauses = []
    if value:
        literal = variable
    else:
        literal = -variable
    for c in clauses:
        if literal in c:
            pass
        elif -literal in c:
            clause_copy = c.copy()
            clause_copy.remove(-literal)
            new_clauses.append(clause_copy)
        else:
            new_clauses.append(c)
    return new_clauses


if __name__ == "__main__":
    f = Dimacs(6, 12)
    print(f.to_string())
    result = solve_dpll(f)
    print(result)
