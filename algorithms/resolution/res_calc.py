def resolve(i, clause_1, clause_2):
    clause_1.discard(0)
    clause_2.discard(0)
    # print('resolving..')
    if (i in clause_1) == (-i in clause_2) and (-i in clause_1) == (i in clause_2):
        # print('checked')
        pass
    else:
        return 'error'
    if i in clause_2:
        temp = clause_1
        clause_1 = clause_2
        clause_2 = temp
        # print('swapped')
    # remove i and -i from clause
    clause_1.remove(i)
    clause_2.remove(-i)
    # print('removed', clause_1, clause_2)
    # combine clauses
    resolvent = clause_1 | clause_2
    # print(resolvent)
    # check for empty clause
    if resolvent == set():
        return 'false'
    # check for tautology
    for x in resolvent:
        if -x in resolvent:
            return 'taut'
    return resolvent


def old_resolve(i, p1, n1, nbvar):
    import algorithms.resolution.sat_solver as solving
    # combine clauses
    r1 = p1 + n1
    # remove positive and negative occurrences
    solving.remove_from_clause([0, i, -i], r1)
    # otherwise clean the clause up (x or notx <=> 1, x or x <=> x), possibly returning 'taut' if it
    # is recognized that r1 is a tautology
    r1 = solving.clean_up_clause(nbvar, r1)
    if r1 == "taut":
        # print('DP:taut')
        return 'taut'
    # check if inconsistency is proven
    if not r1:
        print(p1, n1, "|-", [], "Inconsistent!")
        return False
    # r1.append(0)
    r1.add(0)
    # the resulting clause is added to the result list
    return r1


def main():
    print(resolve(3, {-1, 2, 3}, {-3, -5, 2, -2}))


if __name__ == "__main__":
    main()
