import gen.formats as formats


def qbf_naive(qdimacs_formula=formats.Qdimacs()):
    print("QBF Naive Solving")
    print(qdimacs_formula.to_string())
    for c in qdimacs_formula.clauses:
        c.remove(0)
    result = solve(1, qdimacs_formula.nbvar, qdimacs_formula.quant_set, qdimacs_formula.clauses)
    print(result)
    return result


def solve_clauses(clauses):
    for c in clauses:
        if not clause_value(c):
            return False
    return True


def clause_value(clause):
    for literal in clause:
        if literal:
            return True
    return False


def solve(x, nbvar, quant_set, clauses):
    if x > nbvar:
        return solve_clauses(clauses)
    # for variable x in the qbf formula
    # build extended formula
    # print(clauses, "-->")
    clauses_1 = []
    clauses_2 = []
    for clause in clauses:
        if x in clause:
            dummy = clause.copy()
            dummy.remove(x)
            clauses_1.append(dummy + [True])
            clauses_2.append(dummy + [False])
        elif -x in clause:
            dummy = clause.copy()
            dummy.remove(-x)
            clauses_1.append(dummy + [False])
            clauses_2.append(dummy + [True])
        else:
            clauses_1.append(clause)
            clauses_2.append(clause)
        if [] in clauses or [] in clauses_1 or [] in clauses_2:
            print("WAT")
    value_1 = solve(x + 1, nbvar, quant_set, clauses_1)
    value_2 = solve(x + 1, nbvar, quant_set, clauses_2)
    mode = 'e'
    for quant in quant_set:
        if x in quant:
            mode = quant[0]
    if mode == 'a':
        return value_1 and value_2
    elif mode == 'e':
        return value_1 or value_2


def main():
    f = formats.Qdimacs(5, 3, 5, 3)
    # f.import_file(path_util.get_project_path() + "/files/dimacs/dimacs_0.cnf")
    qbf_naive(qdimacs_formula=f)


if __name__ == "__main__":
    main()
