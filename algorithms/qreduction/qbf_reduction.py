from algorithms.qresolution2.q_solve import Q_Solve
from gen.formats import Qdimacs


# UNUSED SO FAR

class QBF_Reduction:
    q_problem = None
    solution = None

    def __init__(self, problem=Qdimacs()):
        self.q_problem = Q_Solve(problem)
        self.solution = []

    def qbf_reduction(self):
        return self.solve(1, self.q_problem)

    def solve(self, current, q_problem):
        if current >= q_problem.nbvar:
            pass
        else:
            new = QBF_Reduction
