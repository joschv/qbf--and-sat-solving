from pathlib import Path


def get_project_src() -> Path:
    return Path(__file__).parent.parent


def get_project_root() -> Path:
    return Path(__file__).parent.parent


def get_project_path() -> str:
    return str(Path(__file__).parent.parent)
