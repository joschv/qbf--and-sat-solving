from deploytests import sat_solver, formats, util_test, qbf_solver


def run_all_modules():
    # module algorithms

    # module algorithms -> resolution
    from algorithms.resolution import res_calc, sat_solver
    res_calc.main()
    # Skip dp_res, called by sat_solver
    sat_solver.main()

    # module algorithms -> cryptominisat
    # skip, because it was implemented locally only

    # module algorithms -> qreduction
    from algorithms.qreduction import qbf_naive
    qbf_naive.main()
    # Skip qbf_reduction, unfinished module

    # module algorithms -> qresolution
    # note: first/old qbf solver implementation
    from algorithms.qresolution import q_res, qbf_solver
    q_res.main()
    qbf_solver.main()

    # module algorithms -> qresolution2
    # note: second/new qbf solver implementation
    from algorithms.qresolution2 import q_solve
    q_solve.main()

    # module benchmarking
    from benchmarking import series, track, timer
    # Skip benching.main(), highly file dependent
    series.main()
    track.main()
    timer.main()


if __name__ == "__main__":
    if not sat_solver.test(20):
        exit(100)
    if not formats.formats_tests():
        exit(101)
    if not util_test.util_test():
        exit(102)
    if not qbf_solver.q_solve_test():
        exit(103)
    # run all runnable files
    # check if they run without error
    run_all_modules()
